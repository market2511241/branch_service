package postgres

import (
	"context"
	"fmt"
	"market/branch_service/genproto/branch_service"

	"github.com/jackc/pgx/v5/pgxpool"
)

type BranchProductRepo struct {
	db *pgxpool.Pool
}

func NewBranchProductRepo(db *pgxpool.Pool) *BranchProductRepo {
	return &BranchProductRepo{
		db: db,
	}
}

func (r *BranchProductRepo) Create(ctx context.Context, req *branch_service.BranchProductCreateReq) (*branch_service.BranchProductCreateResp, error) {
	query := `
	INSERT INTO branch_products (
		product_id,
		branch_id,
		count
	)
	VALUES ($1,$2,$3);
	`

	_, err := r.db.Exec(ctx, query,
		req.ProductId,
		req.BranchId,
		req.Count,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return nil, err
	}

	return &branch_service.BranchProductCreateResp{
		Msg: "branch_product created with id: " + req.ProductId,
	}, nil
}

func (r *BranchProductRepo) GetList(ctx context.Context, req *branch_service.BranchProductGetListReq) (*branch_service.BranchProductGetListResp, error) {
	var (
		filter  = " WHERE deleted_at IS NULL "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		product_id,
		branch_id,
		count,
		created_at::TEXT,
		updated_at::TEXT 
	FROM branch_products `

	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}
	if req.BranchId != "" {
		filter += ` AND branch_id = ` + "'" + req.BranchId + "' "
	}
	if req.ProductId != "" {
		filter += ` AND product_id = ` + "'" + req.ProductId + "' "
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM branch_products` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp := &branch_service.BranchProductGetListResp{}
	for rows.Next() {
		var branchPr = branch_service.BranchProduct{}
		err := rows.Scan(
			&branchPr.ProductId,
			&branchPr.BranchId,
			&branchPr.Count,
			&branchPr.CreatedAt,
			&branchPr.UpdatedAt,
		)

		if err != nil {
			return nil, err
		}
		resp.BranchProducts = append(resp.BranchProducts, &branchPr)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *BranchProductRepo) GetById(ctx context.Context, req *branch_service.BranchProductIdReq) (*branch_service.BranchProduct, error) {
	query := `
    SELECT 
        product_id,
        branch_id,
        count,
        created_at::TEXT,
        updated_at::TEXT 
    FROM branch_products 
    WHERE product_id = $1;`

	var branchPr = branch_service.BranchProduct{}

	err := r.db.QueryRow(ctx, query, req.ProductId).Scan(
		&branchPr.ProductId,
		&branchPr.BranchId,
		&branchPr.Count,
		&branchPr.CreatedAt,
		&branchPr.UpdatedAt,
	)

	if err != nil {
		return nil, err
	}

	return &branchPr, nil
}

func (r *BranchProductRepo) Update(ctx context.Context, req *branch_service.BranchProductUpdateReq) (*branch_service.BranchProductUpdateResp, error) {
	query := `
    UPDATE branch_products 
    SET 
        branch_id = $1,
        count = $2,
		updated_at = NOW()
    WHERE product_id = $3
    `

	_, err := r.db.Exec(ctx, query,
		req.BranchId,
		req.Count,
		req.ProductId,
	)

	if err != nil {
		return nil, err
	}

	return &branch_service.BranchProductUpdateResp{
		Msg: "branch_product updated",
	}, nil
}

func (r *BranchProductRepo) Delete(ctx context.Context, req *branch_service.BranchProductIdReq) (*branch_service.BranchProductDeleteResp, error) {
	query := `
    UPDATE branch_products 
    SET 
        deleted_at = NOW()
    WHERE id = $1;`

	res, err := r.db.Exec(ctx, query,
		req.ProductId,
	)

	if err != nil {
		return nil, err
	}

	if res.RowsAffected() == 0 {
		return nil, fmt.Errorf("branch_product not found")
	}

	return &branch_service.BranchProductDeleteResp{
		Msg: "branch_product deleted",
	}, nil
}
