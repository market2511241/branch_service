package storage

import (
	"context"
	"market/branch_service/genproto/branch_service"
)

type StorageI interface {
	CloseDB()
	Branch() BranchRepoI
	BranchProduct() BranchProductRepoI
}

type BranchRepoI interface {
	Create(ctx context.Context, req *branch_service.BranchCreateReq) (*branch_service.BranchCreateResp, error)
	GetList(ctx context.Context, req *branch_service.BranchGetListReq) (*branch_service.BranchGetListResp, error)
	GetById(ctx context.Context, req *branch_service.BranchIdReq) (*branch_service.Branch, error)
	Update(ctx context.Context, req *branch_service.BranchUpdateReq) (*branch_service.BranchUpdateResp, error)
	Delete(ctx context.Context, req *branch_service.BranchIdReq) (*branch_service.BranchDeleteResp, error)
}

type BranchProductRepoI interface {
	Create(ctx context.Context, req *branch_service.BranchProductCreateReq) (*branch_service.BranchProductCreateResp, error)
	GetList(ctx context.Context, req *branch_service.BranchProductGetListReq) (*branch_service.BranchProductGetListResp, error)
	GetById(ctx context.Context, req *branch_service.BranchProductIdReq) (*branch_service.BranchProduct, error)
	Update(ctx context.Context, req *branch_service.BranchProductUpdateReq) (*branch_service.BranchProductUpdateResp, error)
	Delete(ctx context.Context, req *branch_service.BranchProductIdReq) (*branch_service.BranchProductDeleteResp, error)
}
