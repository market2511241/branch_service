package service

import (
	"context"
	"market/branch_service/config"
	"market/branch_service/gRPC/client"
	"market/branch_service/genproto/branch_service"
	"market/branch_service/pkg/logger"
	"market/branch_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type BranchProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*branch_service.UnimplementedBranchProductServiceServer
}

func NewBranchProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *BranchProductService {
	return &BranchProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *BranchProductService) Create(ctx context.Context, req *branch_service.BranchProductCreateReq) (*branch_service.BranchProductCreateResp, error) {
	u.log.Info("====== BranchProduct Create ======", logger.Any("req", req))

	resp, err := u.strg.BranchProduct().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating branch_product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchProductService) GetList(ctx context.Context, req *branch_service.BranchProductGetListReq) (*branch_service.BranchProductGetListResp, error) {
	u.log.Info("====== BranchProduct Get List ======", logger.Any("req", req))

	resp, err := u.strg.BranchProduct().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while getting branch_product list", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchProductService) GetById(ctx context.Context, req *branch_service.BranchProductIdReq) (*branch_service.BranchProduct, error) {
	u.log.Info("====== BranchProduct Get by Id ======", logger.Any("req", req))

	resp, err := u.strg.BranchProduct().GetById(ctx, req)
	if err != nil {
		u.log.Error("error while getting branch_product by id", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchProductService) Update(ctx context.Context, req *branch_service.BranchProductUpdateReq) (*branch_service.BranchProductUpdateResp, error) {
	u.log.Info("====== BranchProduct Update ======", logger.Any("req", req))

	resp, err := u.strg.BranchProduct().Update(ctx, req)
	if err != nil {
		u.log.Error("error while updating branch_product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *BranchProductService) Delete(ctx context.Context, req *branch_service.BranchProductIdReq) (*branch_service.BranchProductDeleteResp, error) {
	u.log.Info("====== BranchProduct Delete ======", logger.Any("req", req))

	resp, err := u.strg.BranchProduct().Delete(ctx, req)
	if err != nil {
		u.log.Error("error while deleting branch_product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
