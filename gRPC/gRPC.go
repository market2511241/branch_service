package grpc

import (
	"market/branch_service/config"
	"market/branch_service/gRPC/client"
	"market/branch_service/gRPC/service"
	"market/branch_service/genproto/branch_service"
	"market/branch_service/pkg/logger"
	"market/branch_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	branch_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	branch_service.RegisterBranchProductServiceServer(grpcServer, service.NewBranchProductService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
